#include <nil/crypto3/algebra/curves/pallas.hpp>

constexpr int v_size = 2;
constexpr int security_param = 3;

#include <nil/crypto3/algebra/curves/pallas.hpp>
using namespace nil::crypto3::algebra::curves;

typedef typename pallas::base_field_type::value_type field_type;

[[circuit]] bool
l_inf_norm(
    std::array<
        std::array<field_type, security_param>,
        v_size
    > random_matrix_01,
    std::array<field_type, security_param> norm_vector,
    [[private_input]] std::array<field_type, v_size> random_vector_01,
    [[private_input]] std::array<field_type, v_size> target_vector
)
{
    bool acc = true;
    field_type res;
    for (int i = 0; i < security_param; i++) {
        res = target_vector[0] * (random_vector_01[0] - random_matrix_01[0][i]);
        for (int j = 1; j < v_size; j++) {
            res += target_vector[j] * (random_vector_01[j] - random_matrix_01[j][i]);
        }
        #ifdef __ZKLLVM__
            __builtin_assigner_print_native_pallas_field(res);
            __builtin_assigner_print_native_pallas_field(norm_vector[i]);
        #endif
        acc = ((res == norm_vector[i]) && acc);
    }

    return acc;
}