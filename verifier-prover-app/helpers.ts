import { PRIME_NUMBER } from "./constants";
import * as galois from '@guildofweavers/galois';

function getRandomBoundedUInt(bound: number) {
    return Math.floor(Math.random() * (bound + 1));
}

export function generateLInfBoundedVector(size: number, bound: number) {
    let arr = new Array<bigint>(size);

    for (let i = 0; i < size; i++) {
        arr[i] = BigInt(getRandomBoundedUInt(bound));
    }

    return arr;
}