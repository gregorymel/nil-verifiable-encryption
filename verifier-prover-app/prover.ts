import { PRIME_NUMBER, VECTOR_SIZE, SECURITY_PARAM } from "./constants";
import { createPrimeField } from '@guildofweavers/galois';
import { generateLInfBoundedVector } from "./helpers";
import { serializeMatrix, serializeArray, serializeVector } from "./serialization";
import { writeFileSync } from "fs";

const FIELD = createPrimeField(PRIME_NUMBER);

function generateRandom01Matrix(rows: number, columns: number) {
    var matrix = new Array<Array<number>>(rows);

    for (let i = 0; i < rows; i++) {
        matrix[i] = new Array<number>(columns);
        for (let j = 0; j < columns; j++) {
            matrix[i][j] = Math.round(Math.random()); // {0,1}
        }
    }

    return matrix;
}

function generateRandom01Vector(size: number) {
    var arr = Array<number>(size);

    for (let i = 0; i < size; i++) {
        arr[i] = Math.round(Math.random());
    }

    return arr;
}

function calculateNormVector(targetVector: bigint[], randomMatrix: number[][], randomVector: number[]) {
    let normVector = new Array<bigint>(randomVector.length);
    for (let i = 0; i < randomMatrix[0].length; i++) {
        normVector[i] = BigInt(0);
        for (let j = 0; j < targetVector.length; j++) {
            normVector[i] = FIELD.add(
                normVector[i],
                FIELD.mul(
                    targetVector[j],
                    BigInt(randomVector[j] - randomMatrix[j][i])
                )
            );
        }
    }

    return normVector;
}

function generateCircuitInputFile() {
    const matrix01 = generateRandom01Matrix(VECTOR_SIZE, SECURITY_PARAM);
    const vector01 = generateRandom01Vector(VECTOR_SIZE);
    const targetVector = generateLInfBoundedVector(VECTOR_SIZE, 30);
    const normVector = calculateNormVector(targetVector, matrix01, vector01);

    const result = [
        serializeMatrix(matrix01),
        serializeArray(normVector),
        serializeArray(vector01),
        serializeArray(targetVector)
    ]
    const str = JSON.stringify(result, null, 2);
    writeFileSync("src/main-input.json", str);
    writeFileSync("norm_vector.json", JSON.stringify(serializeVector(normVector), null, 2));
}

generateCircuitInputFile();