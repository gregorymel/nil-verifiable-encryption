export function serializeArray(arr: (bigint | number)[]) {
    const serializedArr = arr.map(e => {
        const bigE = BigInt(e);
        return { "field" : `0x${bigE.toString(16)}` };
    });

    return {
        "array": serializedArr
    };
}

export function serializeMatrix(matrix: (bigint | number)[][]) {
    const serializedMatrix = matrix.map(row => {
        return serializeArray(row);
    });

    return {
        "array": serializedMatrix
    };
}

export function serializeVector(vector: bigint[]) {
    return vector.map(e => {
        return `0x${e.toString(16)}`
    })
}

export function deserializeVector(vector: string[]) {
    return vector.map(e => {
        return BigInt(e);
    })
}