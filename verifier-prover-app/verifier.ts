import { PRIME_NUMBER, VECTOR_SIZE, SECURITY_PARAM } from "./constants";
import { createPrimeField } from '@guildofweavers/galois';
import { readFileSync } from "fs";
import { deserializeVector } from "./serialization";

const FIELD = createPrimeField(PRIME_NUMBER);

function checkLInfNorm(normVector: bigint[], bound: number) {
    var maxVal = BigInt(0);

    for (const value of normVector) {
        var emebddedVal = value;
        // embedding to interval [ -q/2, q/2]
        if (value > (FIELD.characteristic / BigInt(2))) {
            emebddedVal = FIELD.characteristic - value;
        }

        if (emebddedVal > maxVal) {
            maxVal = emebddedVal;
        }
    }

    return maxVal < bound;
}

const str = readFileSync("norm_vector.json").toString();
const vector = deserializeVector(JSON.parse(str));

const isVerified = checkLInfNorm(vector, 30);
console.log(isVerified);