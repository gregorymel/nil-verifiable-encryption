See https://github.com/NilFoundation/zkllvm-template/tree/master to find the template on which this project is based.


# About PVSS

PVSS is a framework for building various multiparty protocols, such as DKG (decentralized key generation), RNG (random number generation), and more. The main advantage of PVSS over other VSS schemes is public verifiability. This means that the correctness of the scheme can be verified by anyone using only public parameters, without revealing any secret information. In a blockchain setting, PVSS correctness can be verified by Smart Contracts.


## How to produce and verify a proof locally

## Step 0: Compile circuit

```bash
scripts/run.sh --docker compile
```


## Step 1: Run prover to generate circuit inputs and public statements for verifier

```bash
npx ts-node verifier-prover-app/prover.ts
```

## Step 2: Build constraints 
```bash
scripts/run.sh --docker build_constraint
```

## Step 2: Build proof 

```bash
scripts/run.sh --docker prove
```

## Step 2: Run verifier to check proof and statements from prover
```bash
npx ts-node verifier-prover-app/verifier.ts
```